import React from 'react';
import PropTypes from 'prop-types';
import { Reply, Sent } from './styles';

const Message = ({ align, content }) => (

  <div>
    { align === 'left' ? (
      <Sent>
        <li>
          <img
            src="http://jump.javin.io/assets/person1.png"
            alt="Sender"
          />
          <p>{content}</p>
        </li>
      </Sent>
    ) : (
      <Reply>
        <li>
          <img
            src="http://jump.javin.io/assets/person2.png"
            alt="Recipient"
          />
          <p>{content}</p>
        </li>
      </Reply>
    )}
  </div>
);

export default Message;

Message.propTypes = {
  content: PropTypes.string.isRequired,
  align: PropTypes.string.isRequired,
};
