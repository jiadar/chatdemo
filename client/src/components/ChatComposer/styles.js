import styled from 'styled-components';

export const StyledChatComposer = styled.div`
  grid-area: composer;
  position: relative;
  bottom: 0;
  width: 100%;
  z-index: 99;
`;

export const InputBar = styled.div`
  position: relative;
  width: 100%;

  input {
    float: left;
    border: none;
    width: calc(100% - 90px);
    padding: 11px 32px 10px 8px;
    font-size: 0.8em;
    color: #32465a;
  }

  @media screen and (max-width: 735px) {
    padding: 15px 32px 16px 8px;
  }

  button {
    float: right;
    border: none;
    width: 50px;
    padding: 12px 0;
    cursor: pointer;
    background: #32465a;
    color: #f5f5f5;
    @media screen and (max-width: 735px) {
      padding: 16px 0;
    }
    &:hover {
      background: #435f7a;
    }
    &:focus {
      outline: none;    
    }
`;
