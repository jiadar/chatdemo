import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { useMutation } from '@apollo/react-hooks';
import { StyledChatComposer, InputBar } from './styles';
import ADD_MESSAGE from '../../graphql/Messages/mutations.gql';

// The chat composer will let you write a message in a form input, and
// add it to the database with a mutation. This will trigger a subscription
// in the message pane to push the message
const ChatComposer = ({ sender, recipient }) => {
  let input;
  const [addMessage] = useMutation(ADD_MESSAGE);

  const submitHandler = (event) => {
    event.preventDefault();
    addMessage({
      variables: {
        message: {
          text: input.value,
          senderId: sender._id,
          recipientId: recipient._id,
        },
      },
    });
    input.value = '';
  };

  return (
    <StyledChatComposer>
      <InputBar>
        <form
          autoComplete="off"
          onSubmit={submitHandler}
        >
          <input
            id="message"
            type="text"
            placeholder="Write your message..."
            ref={(node) => { input = node; }}
          />
          <button
            type="submit"
            className="f6 link dim br3 ph3 pv2 mb2 dib white bg-dark-blue"
            href="#0"
          >
            <FontAwesomeIcon icon={faPaperPlane} />
          </button>
        </form>
      </InputBar>
    </StyledChatComposer>
  );
};

export default ChatComposer;

ChatComposer.propTypes = {
  sender: PropTypes.object.isRequired,
  recipient: PropTypes.object.isRequired,
};
