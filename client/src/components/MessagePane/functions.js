// NOTE: The JSON for the bot messages is deliberately poorly formed to
// demonstrate some logic in these functions

// Calculate the path to the next bot message given a particular response.
// If the path is an object, we return the path based on the response being
// the hash key. Otherwise, the path is a number which we return.
function calculatePath(botMessage, response) {
  return typeof botMessage.paths === 'object'
    ? botMessage.paths[response]
    : botMessage.paths;
}

// Validate that the response input occurs in a list. We reduce the list
// to a boolean value based on receiving a match.
function validateArray(botMessage, response) {
  if (Array.isArray(botMessage.validation)) {
    return botMessage.validation.reduce((res, cur) => {
      if (cur === response) return true;
      return res;
    }, false);
  }
  return false;
}

// Validate the response input matches a regex.
function validateRegex(botMessage, response) {
  if (typeof botMessage.validation === 'string'
      || botMessage.validation instanceof String) {
    const regex = RegExp(botMessage.validation);
    return regex.test(response);
  }
  return false;
}

// Boolean validation check
function validateBoolean(botMessage) {
  if (typeof botMessage.validation === 'boolean') {
    return true;
  }
  return false;
}

// Look up a bot message by it's Id and return the array index. If we don't
// find it, we are at the beginning of the questions, so return 1.
export function lookupBotMessageById(botMessages, id) {
  for (let i = 0; i < botMessages.length; i += 1) {
    if (botMessages[i].id === id) return i;
  }
  return 1;
}

// Validate the response and calculate the next path. Calculate the
// next path based on the message and the user's response. If the response
// is invalid we simply ask the question again. More sophisticated handling
// could occur here as necessary. If the response was valid, return the next
// path. While we have an error in the return object it is currently unused.
export function validateResponse(botMessage, response) {
  let nextId = calculatePath(botMessage, response);
  const isValid = validateBoolean(botMessage)
    || validateArray(botMessage, response)
    || validateRegex(botMessage, response);

  if (!isValid) nextId = botMessage.id;

  return {
    error: false,
    data: {
      nextMessage: nextId,
    },
  };
}
