import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { ApolloClient } from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from '@apollo/react-hooks';
import { WebSocketLink } from 'apollo-link-ws';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import * as serviceWorker from './serviceWorker';
import AppContent from './App';

// Set up internals of apollo server, cache, and subscriptions.
// Send graphql queries to :4000 as well as websocket gateway and
// split based on operation type. Configure the cache as required
// or leave as default. Enable dev tools and wrap the apollo
// client into the react app.
require('dotenv').config();

const wsServerUri = `ws://${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}`;
const apiServerUri = `http://${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/api`;

console.log(process.env);
console.log(`wsServerUri: ${wsServerUri}`);
console.log(`apiServerUri: ${apiServerUri}`);

const httpLink = new createHttpLink({ uri: apiServerUri }); // eslint-disable-line

const wsLink = new WebSocketLink({
  uri: wsServerUri,
  options: {
    reconnect: true,
  },
});

const link = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition'
        && definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const cache = new InMemoryCache();

const client = new ApolloClient({
  link,
  cache,
  connectToDevTools: true,
});

const App = () => (
  <ApolloProvider client={client}>
    <AppContent />
  </ApolloProvider>
);

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
