from pymongo import MongoClient

# should set with env vars
def get_client(host):
    print("Seeding mongodb at " + host + "...")
    MONGO_URI = "mongodb://root:example@" + host + ":27017"
    return MongoClient(MONGO_URI)
