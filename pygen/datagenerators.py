# datagenerators.py
#
# Generates fake data from the faker library for various collection types. 
#

import time
import random
from pprint import pprint
from datetime import datetime
from datetime import timedelta
from pydash import py_
from bson.objectid import ObjectId

# Used for the max sentence length for question. You can change this. 
MAX_SENTENCE_LENGTH = 12


def fake_unix_time():
    TODAY = int(time.time()*1000)
    PAST = TODAY - 30*24*3600
    r = random.SystemRandom().randrange(PAST, TODAY);    
    return r/1000;

def fake_user(fake):
    return {
        'fn': fake.first_name(),
        'ln': fake.last_name(),
        'username': fake.user_name(),
        'email': fake.email(),
    }

def fake_chat(fake, users):
    participants = py_.sample_size(users, 2);
    return {
        '_id': ObjectId('5df316de29b28fa9fb6672fc'),
        'participants': participants,
        'lastAccessed': fake_unix_time(),
    }

def fake_message(fake, chat):
    sender = random.randint(0, 1);
    recipient = 1 if sender == 0 else 0
    return {
        'text': fake_text(fake),
        'dateSent': fake_unix_time(),
        'dateReceived': fake_unix_time(),
        'read': fake.boolean(),
        'chat': chat["_id"],
        'senderId': chat["participants"][sender],
        'recipientId': chat["participants"][recipient],
    }

def fake_text(fake):
    return fake.sentence(nb_words=MAX_SENTENCE_LENGTH, variable_nb_words=True, ext_word_list=None);
