export default `
  type User {
    _id: ID!
    username: String
    email: String
    fn: String
    ln: String
    chats: [Chat]
  }

  type Query {
    user(id: String): User
    users: [User]
  }

  type Mutation {
    addUser(user: UserInput): User
  }

  input UserInput {
    username: String
    email: String
    fn: String
    ln: String
  }    
`;
