export default `
  type Chat {
    _id: ID!
    participants: [User]
    messages: [Message]
    lastAccessed: String
  }

  type Query {
    chat(id: String): Chat
    chats: [Chat]
  }

  type Mutation {
    addChat(chat: ChatInput): Chat
  }

  input ChatInput {
    recipient: String
    lastAccessed: String
  }    
`;
