// read mongo uri from dot env file

require('dotenv').config();

const mongodb = process.env.MONGO_URI;

export default {
  mongodb,
};

